vaeite : SAT-based Solver for Claim-Augmented Argumentation Frameworks
======================================================================

Version    : 2020.03.24

Author     : [Andreas Niskanen](mailto:andreas.niskanen@helsinki.fi), University of Helsinki


Compiling
---------
Please choose desired SAT solver (glucose, glucose-inc) by uncommenting the corresponding line in the Makefile.

To compile ``vaeite``:
```
make
```

To remove all object files:
```
make clean
```


Usage
-----
```
./vaeite -p <task> -f <file> [-a <query>]

  <task>      computational problem
  <file>      input SETAF in APX format
  <query>     query argument
Options:
  --help      Displays this help message.
  --version   Prints version and author information.
  --formats   Prints available file formats.
  --problems  Prints available computational tasks.
```


Input format
------------
For input CAFs, ``vaeite`` uses the APX syntax described on the [ASPARTIX webpage](https://www.dbai.tuwien.ac.at/research/argumentation/aspartix/caf.html):
```
            arg(a,c).    ... a is an argument with claim c
            att(a,b).    ... argument a attacks argument b
```
If the CAF is well-formed (arguments with the same claim attack the same arguments), you may use the following shorthand:
```
            arg(a,c).    ... a is an argument with claim c
            att(x,b).    ... all arguments with claim x attack argument b
```
See ``example.apx`` for an example CAF and ``example_wf.apx`` for an example well-formed CAF.


Dependencies
------------
SAT solver [Glucose](https://www.labri.fr/perso/lsimon/glucose/) (version 4.1) is included in this release.


Contact
-------
Please direct any questions, comments, bug reports etc. directly to [the author](mailto:andreas.niskanen@helsinki.fi).
