/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <iostream>

#include "AF.h"

using namespace std;

AF::AF() : args(0), claims(0), count(0) {}

void AF::add_argument_with_claim(string arg, string claim)
{
	if (arg_to_int.count(arg) == 0) {
		int_to_arg.push_back(arg);
		arg_to_int[arg] = args++;
		arg_to_claim.push_back(0);
		if (claim_to_int.count(claim) == 0) {
			int_to_claim.push_back(claim);
			arg_to_claim[arg_to_int[arg]] = claims;
			claim_to_int[claim] = claims++;
		} else {
			arg_to_claim[arg_to_int[arg]] = claim_to_int[claim];
		}
	} else {
		cerr << "Fatal error: Two arguments have the same identifier " << arg << ".\n";
		exit(1);
	}
}

void AF::add_attack(pair<string,string> att)
{
	if (arg_to_int.count(att.first) != 0 && arg_to_int.count(att.second) != 0) {
		well_formed = false;
		uint32_t source = arg_to_int[att.first];
		uint32_t target = arg_to_int[att.second];
		attackers[target].push_back(source);
	} else if (claim_to_int.count(att.first) != 0 && arg_to_int.count(att.second) != 0) {
		uint32_t source_claim = claim_to_int[att.first];
		uint32_t target = arg_to_int[att.second];
		claim_attackers[target].push_back(source_claim);
		for (uint32_t i = 0; i < claim_to_args[source_claim].size(); i++) {
			attackers[target].push_back(claim_to_args[source_claim][i]);
		}
	} else {
		cerr << "Fatal error: att(" << att.first << "," << att.second << ") contains an invalid indentifier.\n";
		exit(1);
	}
}

void AF::init_claims()
{
	claim_to_args.resize(claims);
	for (uint32_t i = 0; i < args; i++) {
		claim_to_args[arg_to_claim[i]].push_back(i);
	}
}

void AF::init_attackers()
{
	attackers.resize(args);
	claim_attackers.resize(args);
}

void AF::init_vars()
{
	claim_var.resize(claims);
	arg_var.resize(args);
	attacked_by_accepted_var.resize(args);

	for (uint32_t i = 0; i < claims; i++) {
		claim_var[i] = ++count;
	}
	
	for (uint32_t i = 0; i < args; i++) {
		arg_var[i] = ++count;
	}

	if (sem == STG || sem == SST) {
		range_var.resize(args);
		for (uint32_t i = 0; i < args; i++) {
			range_var[i] = ++count;
		}
	}

	for (uint32_t i = 0; i < args; i++) {
		attacked_by_accepted_var[i] = ++count;
	}
}